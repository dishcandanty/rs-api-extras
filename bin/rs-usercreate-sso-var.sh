#!/bin/bash 
# Very Much Not Done
# Error Return Codes
# 1 - User Creation Failed
# 2 - Authentication Failed
# 3 - Failed to Get Provider
# 4 - Unable to Update Permission

#new_user_firstname='davin'
#new_user_lastname='walker'
#new_user_email=''
#new_user_company=''
#new_user_phone=''
new_user_roles='server_login'
#new_user_id='INVALID'
# Variables -------------------------
rs_api_account_master_id="59340"
rs_api_account_id="64719"
rs_api_user="dishcandanty@gmail.com"
rs_api_password=""
rs_api_cookie="rs-cookie"

# Configuration -----------
configuration(){
echo "Credentials"
echo "New User Information"
read -rp 'FirstName: ' new_user_firstname
read -rp 'LastName: ' new_user_lastname
read -rp 'Email Address: ' new_user_email
read -rp 'Company: ' new_user_company
read -rp 'Phone Number(ie 18667200208): ' new_user_phone

read -rp "On which account do you want to assign permissions to? (Current: '$rs_api_account_master_id', Dishcandanty2:64719): " rs_api_account_id
read -rp "Do you want to assign actor? (y/n):" role_actor
read -rp "Do you want to assign publisher? (y/n):" role_publisher
read -rp "Do you want to assign designer? (y/n):" role_designer
read -rp "Do you want to assign server_login? (y/n):" role_server_login
read -rp "Do you want to assign library? (y/n):" role_library
read -rp "Do you want to assign admin? (y/n):" role_admin

}
# Configuration --------------------------------

# Authentication -----------------------------
auth()
{
auth_output=$(curl -v -H 'X_API_VERSION: 1.5' -c "$rs_api_cookie" -X POST -d email="$rs_api_user" -d password="$rs_api_password" -d account_href="/api/accounts/$@" https://my.rightscale.com/api/session 2>&1)

case $auth_output in
  *"204"*)
  echo "Authentication Successful"
  ;;
  *)
  echo "'$auth_output'"
  echo "Authentication Failed"
  exit 2
  ;;
  esac
echo "Auth Finished"
}
# Authentication ------------------------------------------------

# Provider  ------------------------------------------------
getprovider() {
echo "test"
#Assumes there is only one Provider
provider_output=$(curl -v -H 'X-API-VERSION: 1.5' -b $rs_api_cookie https://my.rightscale.com/api/identity_providers 2>1& )

#echo "'$provider_output'"
case $provider_output in
  *"created"*)
    rs_sso_provider_id=$(echo "$provider_output" | sed 's/"//g' | sed -e 's/^.*href:\(.*\),rel.*$/\1/')
    echo "Provider: '$rs_sso_provider_id'"
    ;;
  *)
    echo "Error getting Identity Provider"
    echo "'$provider_output'"
    exit 3
    ;;
esac
}
# Provider  ------------------------------------------------

# User Create ---------------------------------------------------
createuser()
{

create_output=$(curl -v -S -s -b "$rs_api_cookie" -H 'X_API_VERSION:1.5' -X POST \
 -d user[first_name]="$new_user_firstname" \
 -d user[last_name]="$new_user_lastname" \
 -d user[email]="$new_user_email" \
 -d user[company]="$new_user_company" \
 -d user[phone]="$new_user_phone" \
 -d user[identity_provider_href]="$rs_sso_provider_id" \
 -d user[principal_uid]="$new_user_email" \
 "https://my.rightscale.com/api/users" 2>&1)

case $create_output in
	*'201 Created'*)
		echo "Successfully created user"
		new_user_id=$(echo "$create_output" | grep Location | awk '/Location:/ { print $3 }')
		new_user_id=$(echo "$new_user_id" | tr -d '\r')
		echo "--$new_user_id--"
		;;
	*) 
		echo "User Creation Failed"
                echo "Output: '$create_output'"
		exit 1
		;;
esac
}
# User Create -----------------------------------------------------


# Permissions - Create -----------
permission_create(){
permission_output=$(curl -v -S -s -b $rs_api_cookie -H 'X_API_VERSION:1.5' -X POST \
 -d permission[user_href]="https://my.rightscale.com/$new_user_id" \
 -d permission[role_title]="$@" \
"https://my.rightscale.com/api/permissions/" 2>&1)

case $permission_output in
	*'201 Created'*)
		echo "Success "
		;;
        *'role twice:'*)
                echo "Skip: User already has '$@'"
                ;;
	*) 
		echo "Error: Unable to Update Permission"
                echo "Output: '$permission_output'"
		exit 4
		;;
esac


}

# Permissions - Create --------------------------------------------

#Permissions -----------
permissions(){
auth "$rs_api_account_id"
permission_create "observer"
[[ $role_actor = [Yy] ]] && echo -n "Adding Actor: " && permission_create "actor"
[[ $role_publisher = [Yy] ]] && echo -n "Adding publisher: " && permission_create "publisher"
[[ $role_designer = [Yy] ]] && echo -n "Adding designer: " && permission_create "designer"
[[ $role_server_login = [Yy] ]] && echo -n "Adding server_login: " && permission_create "server_login"
[[ $role_library = [Yy] ]] && echo -n "Adding library: " && permission_create "library"
[[ $role_admin = [Yy] ]] && echo -n "Adding admin: " && permission_create "admin"

}
# Permissions -----------------


# ----------------- Actual Script Process
echo "Starting"
read -rsp "Enter Password for $rs_api_user: " rs_api_password
echo
auth "$rs_api_account_master_id"
getprovider
configuration
createuser
permissions

echo "Success! Summary of Created User"
echo "Name: $new_user_firstname $new_user_lastname"
echo "Email: $new_user_email"
echo "Company: $new_user_company"
echo "Phone: $new_user_phone"
echo -n "Permissions given($rs_api_account_id): "
[[ $role_actor = [Yy] ]] && echo -n "actor "
[[ $role_publisher = [Yy] ]] && echo -n "publisher "
[[ $role_designer = [Yy] ]] && echo -n "designer "
[[ $role_server_login = [Yy] ]] && echo -n "server_login "
[[ $role_library = [Yy] ]] && echo -n "library "
[[ $role_admin = [Yy] ]] && echo -n "admin "
echo

exit 0
