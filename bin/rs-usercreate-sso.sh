#!/bin/bash 
# Very Much Not Done
# Error Return Codes
# 1 - User Creation Failed


# Variables -----------------------

#. "$HOME/.rightscale/rs_api_config.sh"
#. "$HOME/.rightscale/rs_api_creds.sh"


new_user_firstname='davin'
new_user_lastname='walker'
new_user_email=''
USER_PASS=''
new_user_company=''
new_user_phone=''
USER_ROLE='server_login'
#rs_sso_provider_id='https://app.onelogin.com/trust/saml2/http-post/sso/59556'
#rs_sso_provider_id='https://sso.connect.pingidentity.com/sso/sp/ACS.saml2'
rs_sso_provider_id='https://sso.connect.pingidentity.com/sso/sp/ACS.saml2?saasid=ccf4ffe6-7005-4c84-946b-898d182f9338'
SSO_ID='test'
SSO_PRINCIPAL_UID='-d user[principal_uid]='$SSO_UID' \'
new_user_id='INVALID'
# Variables -------------------------

# Configuration -----------
configuration(){
[[ -z "$rs_api_account_id" ]] && echo "Please run rs-config.sh first" && exit 1

}
# Configuration --------------------------------

# Authentication -----------------------------
auth()
{
curl  -H 'X-API-VERSION: 1.0' -c $rs_api_cookie -u $rs_api_user:$rs_api_password https://my.rightscale.com/api/acct/$rs_api_account_id/login
echo "Auth Finished"
}
# Authentication ------------------------------------------------

# Provider  ------------------------------------------------
getprovider() {
#Assumes there is only one Provider
output=$(curl -v -H 'X-API-VERSION: 1.5' -b $rs_api_cookie https://my.rightscale.com/api/identity_providers 2>1& )
rs_sso_provider_id=$(echo "$output" | sed 's/"//g' | sed -e 's/^.*href:\(.*\),rel.*$/\1/')
echo "Provider: '$rs_sso_provider_id'"
}
# Provider  ------------------------------------------------


# User Create ---------------------------------------------------
createuser()
{
output=$(curl -v -S -s -b $rs_api_cookie -H 'X_API_VERSION:1.5' -X POST \
 -d user[first_name]="$new_user_firstname" \
 -d user[last_name]="$new_user_lastname" \
 -d user[email]="$new_user_email" \
 -d user[company]="$new_user_company" \
 -d user[phone]="$new_user_phone" \
 -d user[identity_provider_href]="$rs_sso_provider_id" \
 -d user[principal_uid]="" \
 "https://my.rightscale.com/api/users" )

# 2>&1
case $output in
	*'201 Created'*)
		echo "Successfully created user"
		new_user_id=$(echo "$output" | grep Location | awk '/Location:/ { print $3 }')
		new_user_id=$(echo "$new_user_id" | tr -d '\r')
		echo "--$new_user_id--"
		;;
	*) 
		echo "User Creation Failed"
		exit 1
		;;
esac
}
# User Create -----------------------------------------------------


# ----------------- Actual Script Process
echo "Starting"
configuration
auth
echo "Auth Finished-"
getprovider
sleep 10
createuser
echo "- $new_user_id -"
echo "-d permission[user_href]="https://my.rightscale.com/$new_user_id" "
useroutput=$(curl -v -S -s -b $COOKIE -H 'X_API_VERSION:1.5' -X POST \
 -d permission[user_href]="https://my.rightscale.com/$new_user_id" \
 -d permission[role_title]="observer" \
"https://my.rightscale.com/api/permissions/" 2>&1)
echo "$useroutput"


echo "Success!"

exit 0
