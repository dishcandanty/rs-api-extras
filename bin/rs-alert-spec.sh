#!/bin/bash -e



. "$HOME/.rightscale/rs_api_config.sh"
. "$HOME/.rightscale/rs_api_creds.sh"

rs_api_version="1.0"		# ensure API 1.0
SERVER_TEMPLATE="317663001"

echo "start"
curl -v -b "$rs_api_cookie" -X DELETE -H "X-API-VERSION:1.0" \
 "https://my.rightscale.com/api/acct/$rs_api_account_id/alert_specs/3015377001"

echo "finish"
exit  


 curl -i --data -v -b "$rs_api_cookie" -X POST -H "X-API-VERSION:1.0"  \
 -d "alert_spec[name]=woo sim all cpu I/O wait" \
 -d "alert_spec[description]=All cpus are overloaded with I/O" \
 -d "alert_spec[file]=cpu-all/cpu-wait" \
 -d "alert_spec[variable]=value" \
 -d "alert_spec[condition]=<=" \
 -d "alert_spec[threshold]=40" \
 -d "alert_spec[duration]=15" \
 -d "alert_spec[subject_type]=ServerTemplate" \
 -d "alert_spec[subject_href]=https://my.rightscale.com/api/acct/$rs_api_account_id/server_templates/$SERVER_TEMPLATE" \
 -d "alert_spec[action]=escalate" \
 -d "alert_spec[escalation_name]=warning" \
 "https://my.rightscale.com/api/acct/$rs_api_account_id/alert_specs"
exit

