#!/bin/bash

#Log In Credentials
rs_account=""
rs_user=""
rs_password=""

#Security Group to Query For
security_group_id="178045"

# Local Security Group Settings/Configuration
owner_id=""
security_group="default"

# Ports/Protocols to Open
protocol="tcp"
from_port=2200
to_port=2200

# Login and create authentication cookie
url="https://my.rightscale.com/api/acct/$rs_account/login?api_version=1.0"
curl -s -c "rs_api_cookie.txt" -u "$rs_user":"$rs_password" "$url"

# query the group
group=$(curl  -s -b "rs_api_cookie.txt" -H "X-API-VERSION: 1.0" -X GET "https://my.rightscale.com/api/acct/$rs_account/ec2_security_groups/$security_group_id")

# Logic to see if the local group exists in remote group
echo $group | grep -q "$security_group"
check_test=$?

# If its in there don't do anything, otherwise add
if [ $check_test -eq 0 ]; then
      exit 0
      else
          curl -s -b "rs_api_cookie.txt" -H "X-API-VERSION: 1.0" -X PUT \
              -d ec2_security_group[owner]=$owner_id \
                  -d ec2_security_group[group]=$security_group \
                      -d ec2_security_group[protocol]=$protocol \
                          -d ec2_security_group[from_port]=$from_port \
                              -d ec2_security_group[to_port]=$to_port \
                                  "https://my.rightscale.com/api/acct/$rs_account/ec2_security_groups/$security_group_id"
                                  fi
