#!/bin/sh -e
# Command               Server ID
# rs-get-alert-specs.sh 7984132\
rs_server_id="$1"
if [ -z $rs_server_id ]; then
    echo "Missing Server ID"
    read -rp "Server ID: " rs_server_id
    fi

    echo "$rs_server_id"
. "$HOME/.rightscale/rs_api_config.sh"
. "$HOME/.rightscale/rs_api_creds.sh"

rs_api_version="1.0"		# ensure API 1.0

url="https://my.rightscale.com/api/acct/$rs_api_account_id/servers/$rs_server_id/alert_specs"
echo "GET: $url"

xml=$(curl -s -H "X-API-VERSION: $rs_api_version" -b "$rs_api_cookie" "$url" -X GET)

echo "$xml"


