#!/bin/sh -e

# rs-get-server.sh <server_id> [current] [settings]

[[ ! $1 ]] && echo 'No server ID provided.' && exit 1

. "$HOME/.rightscale/rs_api_config.sh"
. "$HOME/.rightscale/rs_api_creds.sh"


url="https://my.rightscale.com/acct/$rs_api_account_id/clouds/3/ec2_instances/$1"
echo "GET: $url"
server_xml=$(curl -s -X GET -b "$rs_api_cookie" \
 -H "Host: my.rightscale.com" \
 -H "Referer: $url" \
 -H "X-Requested-With: XMLHttpRequest" \
 -H "Connection: keep-alive" \
 "$url/info" )
echo "$server_xml"


