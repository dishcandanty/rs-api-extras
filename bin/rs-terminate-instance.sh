#!/bin/bash -e

# This Scrapes the Dashboard. Isn't recommended for Production

# rs-terminate-instance.sh <instance_id>
. "$HOME/.rightscale/rs_api_config.sh"
. "$HOME/.rightscale/rs_api_creds.sh"

# Set RightScale (public) cloud IDs
#1  US-East
#2  Europe
#3  US-West
#4  Singapore 
#5  Tokyo
#6  Oregon
#7  SA-São Paulo (Brazil)

[[ ! $1 ]] && echo 'No Instance ID provided, exiting.' && exit 1
[[ ! $2 ]] && echo 'No Cloud ID provided, exiting.' && exit 2
cloud=$2
instance_id="$1"
stop_server() {
url="https://my.rightscale.com/acct/$rs_api_account_id/clouds/$cloud/ec2_instances/$instance_id/terminate"
terminate_output=$(curl -v -b "$rs_api_cookie" -X PUT \
-H "Host: my.rightscale.com" \
-H "Origin: https://my.rightscale.com" \
-H "Referer: https://my.rightscale.com/acct/$rs_api_account_id/clouds/$cloud/ec2_instances/$instance_id" \
-d _method=put "$url")
}

# terminate the server
stop_server

