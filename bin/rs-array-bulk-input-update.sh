#! /bin/bash

# rs-array-bulk-input-update.sh <rs_cloud_id> <rs_instance_ids> <array> <rs_input_name> <rs_input_type> <rs_input_value>
# rs-array-bulk-input-update.sh 3 11095565001,10397949001 2132472001 "repo/default/revision" text newvalue


[[ ! $1 ]] && echo 'No RightScale cloud ID provided.' && exit 1
[[ ! $2 ]] && echo 'No Instances Provided' && exit 1
[[ ! $3 ]] && echo 'No Array ID Provided' && exit 1
[[ ! $4 ]] && echo 'No input name provided.' && exit 1
[[ ! $5 ]] && echo 'No input type' && exit 1
[[ ! $6 ]] && echo 'No input value' && exit 1


. "$HOME/.rightscale/rs_api_config.sh"
. "$HOME/.rightscale/rs_api_creds.sh"

rs_cloud_id="$1"
rs_instance_ids="$2"
rs_array_id="$3"
rs_input_name="$4"
rs_input_type="$5"
rs_input_value="$6"
rs_instance_count=`grep -o "," <<<"$rs_instance_ids" | wc -l`
let rs_instance_count++

url="https://my.rightscale.com/acct/$rs_api_account_id/inputs/update_bulk_inputs"
echo "POST: $url"

api_result=$(curl -v -s -S -b "$HOME/.rightscale/rs_dashboard_cookie.txt" \
-X POST \
-H "Referer:https://my.rightscale.com/acct/$rs_api_account_id/server_arrays/$rs_array_id/instances" \
-H "Pragma:no-cache" \
-H "X-Requested-With:XMLHttpRequest" \
-d _method="put" \
-d cloud_id="$rs_cloud_id" \
-d container_id="$rs_array_id" \
-d container_type="server_array" \
-d filter_value="" \
-d filter_type="" \
-d inputs[$rs_input_name][$rs_input_type]="$rs_input_value" \
-d inputs[$rs_input_name][type]="$rs_input_type" \
-d next_server="false" \
-d object_count="$rs_instance_count" \
-d object_ids="$rs_instance_ids" \
-d object_type="instance" \
-d select_total_count="" \
-d total_object_count="$rs_instance_count" \
-d update_current="" \
-d update_next="" \
"$url" 2>&1)

if ! grep -i 'Successfully' <<< $api_result > /dev/null 2>&1; then
     echo "Failure, '$api_result'"
     exit 1
fi

